import React from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveWidth} from '../Themes/Metrics'
import HomeTile from './HomeTile'
import Colors from '../Themes/Colors'

const HomeTilesBar = (props) => {
  return (
    <View style={{...styles.tilesContainer, ...{marginVertical: props.marginVertical}}}>
      {props?.homeMenus.map((homeMenu) => {
        if (!!homeMenu?.icon && !!homeMenu?.page_name) {
          let numberShow = ''
          // points show
          homeMenu?.page_layout === 'special_view' && homeMenu?.special_page === 'points'
            ? (numberShow = props?.userInfo?.member?.points)
            : 0
          // saved show
          homeMenu?.page_layout === 'special_view' && homeMenu?.special_page === 'saved'
            ? (numberShow = '$' + props?.userInfo?.member?.saved.toFixed(2).toString())
            : '$0.00'
          // balance show
          homeMenu?.page_layout === 'special_view' && homeMenu?.special_page === 'balance'
            ? (numberShow = '$' + props?.userInfo?.member?.balance)
            : '$0.00'
          // tickets show
          homeMenu?.page_layout === 'special_view' && homeMenu?.special_page === 'tickets'
            ? (numberShow = props?.userInfo?.member?.numberOf?.tickets)
            : 0
          //vouchers show
          homeMenu?.page_layout === 'special_view' && homeMenu?.special_page === 'vouchers'
            ? (numberShow = props?.userInfo?.member?.numberOf?.vouchers)
            : 0
          // favor show
          homeMenu?.page_layout === 'special_view' && homeMenu?.special_page === 'favor'
            ? (numberShow = props?.userInfo?.member?.numberOf?.favorite)
            : 0
          // surveys show
          homeMenu?.page_layout === 'special_view' && homeMenu?.special_page === 'surveys'
            ? (numberShow = props?.userInfo?.member?.numberOf?.surveys)
            : 0
          // gift show
          homeMenu?.page_layout === 'single_view' && homeMenu?.pages[0]?.listing_type_id === '5'
            ? (numberShow = props?.userInfo?.member?.numberOf?.gifts)
            : 0
          // promotions show
          homeMenu?.page_layout === 'single_view' && homeMenu?.pages[0]?.listing_type_id === '4'
            ? (numberShow = props?.userInfo?.member?.numberOf?.promotions)
            : 0
          // ourTeam show
          homeMenu?.page_layout === 'single_view' && homeMenu?.pages[0]?.listing_type_id === '6'
            ? (numberShow = props?.userInfo?.member?.numberOf?.ourTeam)
            : 0
          // whatson show
          homeMenu?.page_layout === 'tab_view' &&
          (homeMenu?.pages[0]?.listing_type_id === '1' || homeMenu?.pages[0]?.listing_type_id === '2')
            ? (numberShow =
                props?.userInfo?.member?.numberOf?.regularEvents +
                props?.userInfo?.member?.numberOf?.specialEvents)
            : 0
          // stampcard show
          homeMenu?.page_layout === 'tab_view' &&
          (homeMenu?.pages[0]?.listing_type_id === '7' || homeMenu?.pages[0]?.listing_type_id === '8')
            ? (numberShow = props?.userInfo?.member?.numberOf?.stampCard)
            : 0
          return (
            <HomeTile
              style={{
                backgroundColor:
                  props?.userInfo?.member?.current_preferred_venue_full?.is_preffered_venue_color ===
                    'true' && !!props?.userInfo?.member?.current_preferred_venue_full?.color
                    ? props?.userInfo?.member?.current_preferred_venue_full?.color
                    : Colors.home.menuBackground,
              }}
              key={homeMenu?.id}
              title={homeMenu?.page_name}
              icon={homeMenu?.icon}
              numberShow={numberShow}
              onPress={props?.onPress.bind(this, homeMenu.id)}
            />
          )
        }
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  tilesContainer: {
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(20),
  },
})

export default HomeTilesBar
