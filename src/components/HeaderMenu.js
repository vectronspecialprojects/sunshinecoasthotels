import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Header from './Header';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

function HeaderMenu({title, props}) {
  return (
    <Header
      title={title}
      hasBackButton={false}
      leftComponent={
        <TouchableOpacity
          onPress={() => {
            props.navigation.toggleDrawer();
          }}>
          <MaterialIcons name="menu" color="#fff" size={30} />
        </TouchableOpacity>
      }
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default HeaderMenu;
