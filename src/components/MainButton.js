import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'

import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'

const MainButton = (props) => {
  return (
    <View
      pointerEvents={props.disabled ? 'none' : 'auto'}
      style={{...(props.disabled ? styles.buttonContainerDisabled : styles.buttonContainer), ...props.style}}>
      <TouchableCmp onPress={props.onPress}>
        <View style={styles.button}>
          <Text style={Styles.largeCapBoldText}>{props.title1}</Text>
          {!props.title2 ? null : <Text style={Styles.xSmallNormalText}>{props.title2}</Text>}
        </View>
      </TouchableCmp>
    </View>
  )
}

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: Colors.first,
    borderRadius: 15,
    overflow: 'hidden',
    width: '90%',
    height: 55,
    marginVertical: 5,
  },
  buttonContainerDisabled: {
    backgroundColor: Colors.first,
    borderRadius: 15,
    overflow: 'hidden',
    width: '90%',
    height: 55,
    marginVertical: 5,
    opacity: 0.5,
  },
  button: {
    borderRadius: 15,
    paddingVertical: 12,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
})

export default MainButton
