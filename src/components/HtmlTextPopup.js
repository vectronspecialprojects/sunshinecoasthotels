import React from 'react'
import {View, Text, StyleSheet, ScrollView} from 'react-native'
import {responsiveHeight, responsiveWidth, deviceWidth, deviceHeight} from '../Themes/Metrics'
import Html from '../components/Html'
import Modal from 'react-native-modal'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import ButtonView from './ButtonView'
import {onLinkPress} from './UtilityFunctions'

const HtmlTextPopup = (props) => {
  return (
    <Modal isVisible={props.isVisible} animationIn="zoomIn" animationOut="zoomOut">
      <View style={styles.popupContainer}>
        <View style={styles.headerWrapper}>
          <Text
            style={{
              ...Styles.mediumCapBoldText,
              ...{color: '#444', textTransform: 'uppercase', alignItems: 'flex-start'},
            }}>
            {props.header}
          </Text>
        </View>

        <ScrollView style={styles.htmlWrapper}>
          {!!props.html && (
            <Html html={props.html} textAlign={'center'} color={'#444'} onLinkPress={onLinkPress}/>
          )}
          {!!props.text && <Text style={[Styles.smallNormalText, {color: '#444'}]}>{props.text}</Text>}
        </ScrollView>

        <View style={styles.popupButtonsBar}>
          <ButtonView
            title={'Ok'}
            disabled={false}
            style={{flex: 1, marginHorizontal: responsiveWidth(6)}}
            onPress={props.onOkPress}
          />
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  popupContainer: {
    borderRadius: 12,
    alignSelf: 'center',
    width: responsiveWidth(300),
    maxHeight: deviceHeight() * 0.7,
    maxWidth: deviceWidth() * 0.7,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    overflow: 'hidden',
  },
  headerWrapper: {
    paddingHorizontal: responsiveWidth(10),
    paddingVertical: responsiveHeight(15),
    width: '100%',
    borderBottomWidth: responsiveHeight(1),
    borderBottomColor: '#eee',
  },
  htmlWrapper: {
    paddingHorizontal: responsiveWidth(20),
  },
  popupButtonsBar: {
    width: '100%',
    height: responsiveHeight(50),
    marginVertical: responsiveHeight(10),
  },
})

export default HtmlTextPopup
