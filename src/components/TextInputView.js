import React, {useState} from 'react'
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import Colors from '../Themes/Colors'

export default function TextInputView({
  value,
  onChangeText,
  title,
  secureTextEntry,
  placeholder,
  onSubmitEditing,
  style,
  titleStyle,
  isRequire,
  editable = true,
  inputStyle,
  multiline = false,
  rightIcon,
  messageError,
  textInputStyle,
  keyboardType,
  onPress,
  maxLength,
  refs,
  onFocus,
  onBlur,
  leftIcon,
  messageInfo,
  onEndEditing,
  autoFocus,
  numberOfLines,
  pointerEvents,
  textAlignVertical = 'center',
  scrollEnabled = false,
  placeholderTextColor
}) {
  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={onPress}
      disabled={!onPress}
      activeOpacity={1}>
      {!!title && (
        <Text style={[styles.titleStyle, titleStyle, messageError && {color: Colors.red}]}>
          {title}
          {isRequire && <Text style={{color: Colors.red}}> *</Text>}
        </Text>
      )}
      <View
        style={[styles.textInputContainer, inputStyle, messageError && {borderColor: Colors.red}]}
        pointerEvents={pointerEvents ? pointerEvents : editable ? 'auto' : 'none'}>
        {!!leftIcon && leftIcon}
        <TextInput
          autoFocus={autoFocus}
          value={value}
          ref={refs}
          style={[styles.textInput, textInputStyle, !editable && {color: Colors.gray}]}
          onChangeText={(text) => {
            onChangeText && onChangeText(text)
          }}
          secureTextEntry={secureTextEntry}
          maxLength={maxLength}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor || Colors.defaultPlaceholderTextColor}
          keyboardType={keyboardType}
          underlineColorAndroid="transparent"
          onSubmitEditing={() => {
            if (onSubmitEditing) {
              onSubmitEditing()
            }
          }}
          onBlur={() => {
            !!onBlur && onBlur()
          }}
          onEndEditing={() => {
            if (onEndEditing) {
              onEndEditing()
            }
          }}
          autoCapitalize="none"
          editable={editable && !onPress}
          scrollEnabled={scrollEnabled}
          multiline={multiline}
          numberOfLines={numberOfLines}
          textAlignVertical={textAlignVertical}
          onFocus={() => {
            !!onFocus && onFocus()
          }}
        />
        {!!rightIcon && rightIcon}
      </View>
      {!!messageError && <Text style={styles.error}>{messageError}</Text>}
      {!!messageInfo && !messageError && <Text style={{fontSize: 11}}>{messageInfo}</Text>}
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: responsiveHeight(10),
  },
  textInputContainer: {
    height: responsiveHeight(48),
    borderRadius: responsiveHeight(12),
    minHeight: 40,
    paddingHorizontal: responsiveWidth(12),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.defaultTextInputBackgound,
  },
  textInput: {
    flex: 1,
    fontSize: responsiveFont(15),
    fontFamily: Fonts.openSansBold,
    paddingVertical: responsiveHeight(5),
    color: Colors.defaultTextColor,
    backgroundColor: Colors.defaultTextInputBackgound
  },
  titleStyle: {
    fontSize: responsiveFont(15),
  },
  error: {
    fontSize: responsiveFont(11),
    color: Colors.red,
    fontFamily: Fonts.openSans,
  },
})
