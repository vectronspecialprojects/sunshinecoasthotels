import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Colors from '../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'

function HistoryItem({data}) {
  const {order_details} = data || {}
  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Text style={styles.text}>#{data?.id}</Text>
        <Text style={styles.text}>Total: {data?.total_price}</Text>
      </View>
      {order_details?.map((item, index) => {
        return (
          <View style={styles.row} key={index.toString()}>
            <Text style={[styles.text, {flex: 3}]}>{item?.voucher_name}</Text>
            <View style={[styles.row, {flex: 2, marginBottom: 0}]}>
              <Text style={styles.text}>Price: {item?.unit_price}</Text>
              <Text style={styles.text}>Qty: {item?.qty}</Text>
            </View>
          </View>
        )
      })}
      <View style={styles.row}>
        <Text style={styles.text}>{data?.ordered_at}</Text>
        <Text style={styles.text}>Status: {data?.order_status}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.history.background,
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(12),
    borderRadius: responsiveHeight(10),
    padding: responsiveWidth(10),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: responsiveHeight(12),
  },
  text: {
    color: Colors.history.text,
    fontSize: responsiveFont(12),
    fontFamily: Fonts.openSans,
  },
})

export default HistoryItem
