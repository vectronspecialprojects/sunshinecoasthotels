import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import fonts from '../Themes/Fonts'
import Html from '../components/Html'
import {onLinkPress} from '../components/UtilityFunctions'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'

const FaqItem = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{props.title}</Text>
      <Html
        html={`<div>${props.content}</div>`}
        color={Colors.faq.description}
        textAlign={'left'}
        onLinkPress={onLinkPress}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.faq.background,
    marginHorizontal: responsiveWidth(20),
    marginBottom: responsiveHeight(18),
    padding: responsiveWidth(20),
    borderRadius: responsiveWidth(14),
  },
  title: {
    fontSize: 14,
    color: Colors.faq.title,
    fontFamily: fonts.openSansBold,
  },
})

export default FaqItem
