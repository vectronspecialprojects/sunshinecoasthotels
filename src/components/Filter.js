import React, {useState, useMemo} from 'react'
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native'
import {useSelector} from 'react-redux'
import Modal from 'react-native-modal'
import {responsiveHeight, responsiveWidth, responsiveFont} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import Ionicons from 'react-native-vector-icons/Ionicons'
import {isGroupFilter} from '../constants/env'

const Filter = ({showAllVenue = true, ...props}) => {
  const [selectedTag, setSelectedTag] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const venues = useSelector((state) => state.infoServices.venues)
  const venueTags = useSelector((state) => state.infoServices.venueTags)

  const tagsHandler = (tag) => {
    if (selectedTag === tag.id) {
      setIsOpen(false)
      setSelectedTag(null)
    } else {
      setIsOpen(true)
      setSelectedTag(tag.id)
    }
  }

  const showData = useMemo(() => {
    if (isGroupFilter) {
      let venuesList = []
      venues.forEach((venue) => {
        if (venue.venue_pivot_tags.filter((tag) => tag.venue_tag_id === selectedTag).length > 0)
          venuesList = venuesList.concat(venue)
      })
      return venuesList
    }
    if (showAllVenue) {
      return venues.concat({id: 0, name: 'All Venues'}).sort((a, b) => a.id - b.id)
    }
    return venues.sort((a, b) => a.id - b.id)
  }, [selectedTag, venues, isGroupFilter, showAllVenue])

  const renderItem = (itemData) => {
    return (
      <TouchableOpacity onPress={props.onFilterPress.bind(this, itemData.item)}>
        <View style={styles.listWrapper}>
          {props.selectedId === itemData.item.id && (
            <Ionicons name="ios-checkmark" size={24} color={Colors.defaultFilterText} />
          )}
          <Text style={styles.list}>{itemData.item.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  const renderGroupItem = (itemData) => {
    return (
      <View>
        <TouchableOpacity onPress={tagsHandler.bind(this, itemData.item)}>
          <View style={styles.listWrapper}>
            <Text style={styles.list}>{itemData.item.name}</Text>
          </View>
        </TouchableOpacity>
        {selectedTag === itemData.item.id && isOpen && (
          <TouchableOpacity style={{flex: 1}} onPress={props.backScreenOnPress}>
            <FlatList
              style={{
                paddingLeft: isGroupFilter ? responsiveWidth(10) : 0,
              }}
              data={showData}
              renderItem={renderItem}
              keyExtractor={(item) => item.id.toString()}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }

  if (isGroupFilter) {
    return (
      <Modal
        backdropOpacity={0.2}
        isVisible={props.isVisible}
        animationIn="slideInDown"
        animationOut="slideOutRight">
        <TouchableOpacity style={{flex: 1}} onPress={props.backScreenOnPress}>
          <View style={styles.container}>
            {showAllVenue && (
              <TouchableOpacity onPress={props.onFilterPress.bind(this, {id: 0})}>
                <View style={styles.listWrapper}>
                  {props.selectedId === 0 && (
                    <Ionicons name="ios-checkmark" size={24} color={Colors.defaultFilterText} />
                  )}
                  <Text style={styles.list}>All Venues</Text>
                </View>
              </TouchableOpacity>
            )}
            <FlatList
              data={venueTags}
              renderItem={renderGroupItem}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
        </TouchableOpacity>
      </Modal>
    )
  }

  return (
    <Modal
      backdropOpacity={0.2}
      isVisible={props.isVisible}
      animationIn="slideInDown"
      animationOut="slideOutRight">
      <TouchableOpacity style={{flex: 1}} onPress={props.backScreenOnPress}>
        <View style={styles.container}>
          <FlatList data={showData} renderItem={renderItem} keyExtractor={(item) => item.id.toString()} />
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    width: responsiveWidth(200),
    minHeight: responsiveHeight(100),
    maxHeight: responsiveHeight(250),
    marginTop: responsiveHeight(110),
    marginRight: responsiveWidth(5),
    paddingLeft: responsiveWidth(20),
    paddingVertical: responsiveWidth(10),
    backgroundColor: Colors.defaultFilterBackground,
    alignSelf: 'flex-end',
    borderRadius: responsiveWidth(20),
  },
  listWrapper: {
    flexDirection: 'row',
    marginBottom: responsiveHeight(5),
    alignItems: 'center',
  },
  list: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(14),
    color: Colors.defaultFilterText,
    marginLeft: responsiveWidth(10),
  },
})

export default Filter
