import React, {Component} from 'react'
import {View, StyleSheet, Text, Modal} from 'react-native'
import Emitter from '../utilities/Emitter'
import ButtonView from './ButtonView'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'

export default class Alert extends Component {
  constructor() {
    super()
    this.state = {
      title: '',
      message: '',
      isVisible: false,
      buttons: [],
      type: '',
    }
  }

  /**
   * Show Alert text box
   * @param title title of alert text box
   * @param message message of alert text box
   * @param buttons of type {title: string, primary: bool, onPress: func}
   */
  static alert(title, message, buttons = null, type = 'alert') {
    let actions = buttons
    if (actions === null) {
      actions = [
        {
          text: 'OK',
          primary: true,
          onPress: () => {},
        },
      ]
    }
    Emitter.emit('SHOW_ALERT', {title, message, buttons: actions, type})
  }
  static cancel() {
    Emitter.emit('HIDE_ALERT')
  }

  componentDidMount() {
    Emitter.on('SHOW_ALERT', (data) => {
      this.setState({
        ...data,
        isVisible: true,
      })
    })

    Emitter.on('HIDE_ALERT', (data) => {
      this.setState({
        isVisible: false,
      })
    })
  }

  componentWillUnmount() {
    Emitter.rm('SHOW_ALERT')
  }

  _hide = () => {
    // eslint-disable-next-line no-undef
    requestAnimationFrame(() => {
      this.setState({isVisible: false})
    })
  }

  render() {
    const {isVisible, title, message, buttons, type} = this.state
    return (
      <Modal visible={isVisible} transparent={true} onRequestClose={() => {}}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.3)',
          }}>
          <View style={styles.container}>
            <View style={styles.messageWrapper}>
              {!!title && <Text style={styles.title}>{title}</Text>}
              <Text style={styles.message}>{message}</Text>
            </View>
            <View style={styles.actions}>
              {buttons.map((action, index) => (
                <ButtonView
                  style={{
                    flex: 1,
                    marginHorizontal: 5,
                    ...action.style,
                  }}
                  textStyle={action.textStyle}
                  key={index.toString()}
                  title={action.text}
                  onPress={() => {
                    this._hide()
                    if (action.onPress !== undefined) {
                      action.onPress()
                    }
                  }}
                />
              ))}
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderRadius: 20,
    width: '90%',
    alignItems: 'center',
    overflow: 'hidden',
    paddingBottom: responsiveHeight(15),
  },
  message: {
    marginTop: 5,
    fontSize: responsiveFont(14),
    color: Colors.defaultAlertTextmessage,
    textAlign: 'center',
  },
  messageWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: responsiveWidth(10),
    paddingTop: responsiveHeight(20),
  },
  actions: {
    marginTop: responsiveHeight(30),
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(10),
  },
  primary: {},
  title: {
    color: Colors.defaultAlertText,
    fontSize: responsiveFont(18),
    textAlign: 'center',
    fontWeight: 'bold'
  },
})
