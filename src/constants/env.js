import {Platform} from 'react-native'

const buildEvn = {
  production: 'production',
  staging: 'staging',
}

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: '57e5b637-20dd-4975-bd85-90f153da2030',
  buildEvn: buildEvn.production,
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'PrpH8BG3vDBqh6XR4T52pd3vrIiizStrCiim8',
    production: 'p2QdvqEtUjkyfoIOnWe7Vwzun2A9OD03DePps',
  },
  android: {
    staging: 'pLGd4DEq2RBnooz4L1NLNNz9xw2_aR60U2z0d',
    production: 'VBprvs9MhAeiu6KX-TqdEL0sfMSOkq_oAgJLl',
  },
})

export const venueName = 'Sunshine Coast Hotels'
export const venueWebsite = 'https://www.suncoasthotels.com.au'

//startup page setup
export const isShowLogo = true

//sign up page setup
export const isMultiVenue = true
export const accountMatch = true

export const isGaming = true //need to be true to allow gamingOdyssey or gamingIGT
export const gamingOdyssey = true
export const gamingIGT = false

export const showTierSignup = false //need to be true to allow isCustomField and showMultipleExtended
export const isCustomField = false //need to be true to allow showMultipleExtended
export const showMultipleExtended = false

//home page setup
export const isShowStarBar = false
export const showTierBelowName = false
export const showPointsBelowName = true
export const isShowPreferredVenue = true
export const showTier = false

export const isGroupFilter = false // must be multi venue and has venue tags
export default vars
