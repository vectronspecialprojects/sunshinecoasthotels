import * as act from '../actions/actionCreator'
import {updateObject} from '../../utilities/utils'

const initialState = {
  user_credentials: {},
}

export default (state = initialState, action) => {
  switch (action.type) {
    case act.AUTHENTICATE:
      return updateObject(state, {user_credentials: action.user_credentials})
    case act.LOGOUT:
      return initialState
    default:
      return state
  }
}
