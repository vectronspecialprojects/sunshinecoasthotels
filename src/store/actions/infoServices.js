import * as api from '../../utilities/ApiManage'
import * as act from './actionCreator'
import {checkTokenValid} from './authServices'
import {authenticate, saveDataToStorage} from './authServices'
import {setAccessToken} from '../../utilities/NetworkingAuth'

export const fetchLegals = () => {
  return async (dispatch) => {
    const response = await api.apiGetLegals()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      const legals = []
      for (const key in response.data) {
        legals.push(response.data[key])
      }
      dispatch({
        type: act.GET_LEGALS,
        legals: legals,
      })
    }
  }
}

export const fetchAbout = () => {
  return async (dispatch) => {
    const response = await api.apiGetAbout()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({
        type: act.GET_ABOUT,
        about: response.data.about,
      })
    }
  }
}

export const fetchVenues = () => {
  return async (dispatch) => {
    const response = await api.apiGetVenues()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({
        type: act.GET_VENUES,
        venues: !!response?.data[0].display_order
          ? response?.data.sort((a, b) => a.display_order - b.display_order)
          : response?.data.sort((a, b) => a.id - b.id),
        venueTags: response?.venue_tags,
        venueTagsSetting: {
          bgColor: response?.venue_tag_bgcolor,
          isTagEnabled: response?.venue_tags_enable,
        },
        venuesList: !!response?.data[0].display_order
          ? response?.data
              .concat({id: 0, display_order: 0, name: 'All Venues'})
              .sort((a, b) => a.display_order - b.display_order)
          : response?.data.concat({id: 0, display_order: 0, name: 'All Venues'}).sort((a, b) => a.id - b.id),
      })
    }
  }
}

export const fetchProfile = () => {
  return async (dispatch) => {
    const response = await api.apiGetProfile()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({type: act.GET_PROFILE, profile: response.data})
    }
  }
}

export const fetchVouchers = () => {
  return async (dispatch) => {
    const profile = await api.apiGetProfile()
    if (profile.ok) {
      dispatch({type: act.GET_PROFILE, profile: profile.data})
    }
    const response = await api.apiGetVouchers()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({
        type: act.GET_VOUCHERS,
        vouchers: response.data,
      })
    }
  }
}

export const fetchOffers = () => {
  return async (dispatch) => {
    const response = await api.apiGetOffers()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({
        type: act.GET_OFFERS,
        offers: response.data,
      })
    }
  }
}

export const fetchTiers = () => {
  return async (dispatch) => {
    const response = await api.apiGetTiers()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({
        type: act.GET_TIERS,
        offers: response.data,
      })
    }
  }
}

export const submitFeedback = (rating, message, ifContactMe) => {
  return async () => {
    const response = await api.apiSubmitFeedback(rating, message, ifContactMe)
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      return response.message
    }
  }
}

export const claimOffer = (listing) => {
  return async () => {
    const response = await api.apiClaimOffer(JSON.stringify([listing]))
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      return response.message
    }
  }
}

export const switchFavorite = (listId, favorite) => {
  return async () => {
    const response = await api.apiSwitchFavorite(listId, favorite)
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      return response.message
    }
  }
}

export const submitEnquiry = (listing_id, subject, message) => {
  return async () => {
    const response = await api.apiSubmitEnquiry(listing_id, subject, message)
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      return response.message
    }
  }
}

export const fetchFaq = () => {
  return async (dispatch) => {
    const response = await api.apiGetFaq()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({type: act.GET_FAQ, faq: response.data})
    }
  }
}

export const fetchTickets = () => {
  return async (dispatch) => {
    const response = await api.apiGetTickets()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({type: act.GET_TICKETS, tickets: response.data})
    }
  }
}

export const fetchSurveys = () => {
  return async (dispatch) => {
    const response = await api.apiGetListSurvey()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({type: act.GET_SURVEYS, surveys: Object.values(response.data)})
    }
  }
}

export const fetctListings = (id) => {
  return async (dispatch) => {
    if (id === 7) {
      const profile = await api.apiGetProfile()
      if (profile.ok) {
        dispatch({type: act.GET_PROFILE, profile: profile.data})
      }
    }
    const response = await api.apiGetListing(id)
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      switch (id) {
        case 1:
          dispatch({type: act.GET_SPECIALEVENTS, events: response})
          break
        case 2:
          dispatch({type: act.GET_REGULAREVENTS, events: response})
          break
        case 5:
          dispatch({type: act.GET_GIFTCERTIFICATE, giftcertificate: response})
          break
        case 6:
          dispatch({type: act.GET_OUTTEAM, ourteam: response})
          break
        case 7:
          dispatch({type: act.GET_STAMPCARDS, stampcards: response})
          break
        case 8:
          dispatch({type: act.GET_STAMPCARDSWON, stampcardswon: response})
          break
      }
    }
  }
}

export const updateProfile = (profile) => {
  return async (dispatch, getState) => {
    dispatch(
      checkTokenValid(async (user_credentials) => {
        const response = await api.apiUpdateProfile({
          ...profile,
          access_token: user_credentials?.access_token,
          id_token: user_credentials?.id_token,
        })
        if (!response.ok) {
          throw new Error(response?.message)
        }
        if (response.ok) {
          const updateCredentials = {
            access_token: response.access_token,
            id_token: response.id_token,
            refresh_token: user_credentials.refresh_token,
            token: user_credentials.token,
          }
          setAccessToken(user_credentials.token)
          dispatch(authenticate(updateCredentials))
          saveDataToStorage(updateCredentials)
          dispatch(fetchProfile())
        }
      }),
    )
  }
}

export function getTransactionSuccess(payload) {
  return {
    type: act.GET_TRANSACTION_SUCCESS,
    payload,
  }
}

export function getTransaction() {
  return async (dispatch) => {
    const response = await api.apiGetTransaction()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch(getTransactionSuccess(response.data))
    }
  }
}

function getFavoriteSuccess(payload) {
  return {
    type: act.GET_FAVORITE_SUCCESS,
    payload,
  }
}
export function getFavorite() {
  return async (dispatch) => {
    const response = await api.apiGetFavorite()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch(getFavoriteSuccess(formatData(response.data)))
    }
  }
}

export function updateProfileAvatar(updateImage) {
  return async (dispatch) => {
    const response = await api.uploadProfileAvatar(updateImage.path)
    if (!response.ok) throw new Error(response?.message)
    await dispatch(fetchProfile())
  }
}

function formatData(data) {
  if (!data?.length) return []
  let newData = {}
  data.map((item) => {
    if (!newData[item.listing?.type?.name]) {
      newData[item.listing?.type?.name] = {
        title: item.listing?.type?.name,
        data: [item],
      }
    } else {
      newData[item.listing?.type?.name].data.push(item)
    }
  })
  return Object.values(newData)
}

export const changePreferredVenue = (venueId) => {
  return async () => {
    const response = await api.apiChangePreferredVenue(venueId)
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      return response?.message
    }
  }
}

export const getNotification = () => {
  return async (dispatch) => {
    const response = await api.apiGetNotification()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({type: act.UPDATE_NOTIFICATIONS, notifications: response?.data})
    }
  }
}

export const deleteNotification = (id) => {
  return async (dispatch, getState) => {
    const response = await api.apiDeleteNotification(id)
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      if (id === 0) dispatch({type: act.UPDATE_NOTIFICATIONS, notifications: {}})
      else {
        const notifications = getState().infoServices.notifications
        dispatch({
          type: act.UPDATE_NOTIFICATIONS,
          notifications: notifications.filter((msg) => msg.id !== id),
        })
      }
    }
  }
}

export const fetctReferMsg = () => {
  return async (dispatch) => {
    const response = await api.apiGetReferMsg()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({
        type: act.GET_REFERMSG,
        referMsg: response?.data.friend_referral_message,
      })
    }
  }
}

export const resetPassword = (email) => {
  return async () => {
    const response = await api.apiResetPassword(email)
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      return response?.message
    }
  }
}

export const fetchMembershipList = () => {
  return async (dispatch) => {
    const response = await api.apiMembership()
    if (!response.ok) {
      throw new Error(response?.message)
    }
    if (response.ok) {
      dispatch({
        type: act.GET_MEMBERSHIPLIST,
        membershiplist: response.data,
      })
    }
  }
}

export function fetchAllData() {
  return async (dispatch) => {
    dispatch(fetchAbout())
    dispatch(fetchTickets())
    dispatch(fetchVouchers())
    dispatch(fetchFaq())
    dispatch(fetchOffers())
    dispatch(fetctListings(1))
    dispatch(fetctListings(2))
    dispatch(fetctListings(5))
    dispatch(fetctListings(6))
    dispatch(fetctListings(7))
    dispatch(fetctListings(8))
    dispatch(getTransaction())
    dispatch(getFavorite())
    dispatch(getNotification())
    dispatch(fetctReferMsg())
    dispatch(fetchSurveys())
    dispatch(fetchMembershipList())
  }
}
