import RouteKey from '../navigation/RouteKey';

export default {
  app: {
    showGlobalIndicator: false,
    appStack: RouteKey.SplashScreen,
  },
};
