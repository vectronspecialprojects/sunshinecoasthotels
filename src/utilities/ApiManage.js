import {
  getAccessToken,
  getWithCheckingToken,
  postWithCheckingToken,
  putWithCheckingToken,
} from './NetworkingAuth'
import {API_URI, app_secret, app_token} from '../constants/constants'
import {urlToBlob} from './utils'
import {isIOS} from '../Themes/Metrics'

export function apiLogin(email, password) {
  return postWithCheckingToken(`${API_URI}loginSSO`, {}, {email, password})
}

export function apiSignup(data) {
  return postWithCheckingToken(`${API_URI}signup`, {}, data)
}

export function apiUpdateProfile(profile) {
  return putWithCheckingToken(`${API_URI}member/profile`, {}, profile)
}

export function apiGetLegals() {
  return postWithCheckingToken(`${API_URI}legals`)
}

export function apiGetAbout() {
  return postWithCheckingToken(`${API_URI}about`)
}

export function apiGetVenues() {
  return postWithCheckingToken(`${API_URI}venue`)
}

export function apiGetAppSettings() {
  return postWithCheckingToken(`${API_URI}appSettings`)
}

export function apiGetProfile() {
  return postWithCheckingToken(`${API_URI}member/profile`)
}

export function apiGetVouchers() {
  return postWithCheckingToken(`${API_URI}member/userVoucher`)
}

export function apiGetOffers() {
  return postWithCheckingToken(`${API_URI}member/promotions`)
}

export function apiGetTiers() {
  return postWithCheckingToken(`${API_URI}tiers`)
}

export function apiGetGaming() {
  return postWithCheckingToken(`${API_URI}gamingsetting`)
}

export function apiIsBepozAccountCreated(email) {
  return postWithCheckingToken(`${API_URI}member/isBepozAccountCreated`, {}, {email})
}

export function apiSubmitFeedback(rating, message, contact_me) {
  return postWithCheckingToken(
    `${API_URI}member/feedback`,
    {},
    {
      rating,
      message,
      contact_me,
    },
  )
}

export function apiClaimOffer(promotion_data) {
  return postWithCheckingToken(`${API_URI}member/claimPromotion`, {}, {promotion_data})
}

export function apiSwitchFavorite(listing_id, favorite) {
  return postWithCheckingToken(
    `${API_URI}member/favorite`,
    {},
    {
      listing_id,
      status: favorite ? 'nonfavorite' : 'favorite',
    },
  )
}

export function apiCheckEmail(email) {
  return getWithCheckingToken(`${API_URI}email/check?email=${email}`, {})
}

export function apiSubmitEnquiry(listing_id, subject, message) {
  return postWithCheckingToken(
    `${API_URI}member/enquire`,
    {},
    {
      listing_id,
      subject,
      message,
    },
  )
}

export function apiGetFaq() {
  return postWithCheckingToken(`${API_URI}member/faq`)
}

export function apiGetListing(display_id) {
  return postWithCheckingToken(`${API_URI}member/listing`, {}, {display_id})
}

export function apiGetTransaction() {
  return postWithCheckingToken(`${API_URI}member/transaction`)
}

export function apiGetFavorite() {
  return postWithCheckingToken(`${API_URI}member/favoriteList`)
}

export function apiChangePassword(old_password, new_password) {
  return putWithCheckingToken(`${API_URI}member/password`, {}, {old_password, new_password})
}

export function apiRefreshToken(refresh_token) {
  return postWithCheckingToken(`${API_URI}member/refreshSSO`, {}, {refresh_token})
}

export function apiVerifyIdToken(id_token) {
  return postWithCheckingToken(`${API_URI}member/verifyIDToken`, {}, {id_token})
}

export function apiChangeEmail(email, password) {
  return postWithCheckingToken(`${API_URI}member/profile/email`, {}, {email, password})
}

export function apiGetListSurvey() {
  return postWithCheckingToken(`${API_URI}member/surveyAll`)
}

export function apiGetSurveyDetails(id) {
  return postWithCheckingToken(`${API_URI}member/survey/${id}`)
}

export function submitSurvey(survey) {
  return postWithCheckingToken(`${API_URI}member/survey`, {}, {survey})
}

export async function uploadProfileAvatar(fileUri) {
  let res = await urlToBlob(fileUri)
  let formdata = new FormData()
  formdata.append('app_secret', app_secret)
  formdata.append('app_token', app_token)
  formdata.append('photo', {
    uri: isIOS() ? fileUri : fileUri.replace('file:/', 'file:///'),
    type: res.type,
    name: fileUri.split('/').pop().toLowerCase(),
  })
  return fetch(`${API_URI}member/profile/photo?token=${getAccessToken()}`, {
    method: 'post',
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    body: formdata,
  })
    .then((response) => {
      return response.json().then((data) => {
        return {
          ok: response.ok,
          ...data,
        }
      })
    })
    .catch((err) => {
      return err
    })
}

export function apiGetTickets() {
  return postWithCheckingToken(`${API_URI}member/userTickets`)
}

export function apiSubmitRefer(email, name) {
  return postWithCheckingToken(`${API_URI}member/friendreferral`, {}, {email, name})
}

export function apiChangePreferredVenue(venue_id) {
  return postWithCheckingToken(`${API_URI}member/changePreferredVenue`, {}, {venue_id})
}

export function apiGetNotification() {
  return postWithCheckingToken(`${API_URI}member/notification`)
}

export function apiDeleteNotification(id) {
  return postWithCheckingToken(`${API_URI}member/notification/${id}`)
}

export function apiGetReferMsg() {
  return postWithCheckingToken(`${API_URI}member/referralMessage`)
}

export function createGCOrder(listing, full_name, email, note) {
  return postWithCheckingToken(
    `${API_URI}member/purchaseGiftCertificate`,
    {},
    {
      data: JSON.stringify(listing),
      full_name,
      email,
      note,
    },
  )
}

export function apiGetListCard() {
  return postWithCheckingToken(`${API_URI}member/stripeInfo`, {}, {})
}

//var data = {
//   type: $scope.order.mix ? 'mix' : $scope.order.show,
//   order: JSON.stringify(products),
//   total: JSON.stringify({
//     point: $scope.order.mix
//       ? angular.isDefined($scope.order.pointUse)
//         ? $scope.order.pointUse
//         : 0
//       : $scope.order.show == 'point'
//       ? $scope.getTotal($scope.listing.products, 'point')
//       : 0,
//     cash: $scope.order.show == 'cash' ? $scope.getTotal($scope.listing.products, 'cash') : 0,
//   }),
//   listing_id: $scope.listing.id,
// }

export function apiCreateOrder(data) {
  console.log('apiCreateOrder: ', data)
  return postWithCheckingToken(`${API_URI}member/placeOrder`, {}, data)
}

export function apiConfirmOrder(order_token, option, paypal_id) {
  return postWithCheckingToken(`${API_URI}member/orderConfirm`, {}, {order_token, option, paypal_id})
}

export function sendToken(order_token, stripe_token, card, user_credentials) {
  return postWithCheckingToken(
    `${API_URI}member/stripe`,
    {},
    {
      order_token,
      stripe_token,
      use_saved_card: card.use_saved_card,
      save_card: card.save_card,
      access_token: user_credentials?.access_token,
      id_token: user_credentials?.id_token,
    },
  )
}

export function apiGetStripeKey() {
  return postWithCheckingToken(`${API_URI}member/stripeKey`)
}

export function apiResetPassword(email) {
  return postWithCheckingToken(`${API_URI}reset`, {}, {email})
}

export function getMatchInstruction() {
  return postWithCheckingToken(`${API_URI}instruction`)
}

export function accountSearch(AccountID, AccNumber) {
  return postWithCheckingToken(
    `${API_URI}accountSearch`,
    {},
    {
      AccountID,
      AccNumber,
    },
  )
}

export function mathAccountInfo(data) {
  return postWithCheckingToken(`${API_URI}signupMatch`, {}, data)
}

export function apiSignupIGT(data) {
  return postWithCheckingToken(`${API_URI}signupIGT`, {}, data)
}

export function apiSignupOdyssey(data) {
  return postWithCheckingToken(`${API_URI}signupOdyssey`, {}, data)
}

export function apiMembership() {
  return postWithCheckingToken(`${API_URI}member/membershiptierslist`)
}
