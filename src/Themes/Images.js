export default {
  defaultImage: require('../assets/img/default_image.jpg'),
  maintenance: require('../assets/img/maintenance.png'),
  logo: require('../assets/img/bepoz_logo.png'),
  logoDrawer : require('../assets/img/bepoz_logo.png'),
  logoSplashScreen: require('../assets/img/bepoz_logo.png'),
  logoStartupScreen: require('../assets/img/bepoz_logo.png'),
  background: require('../assets/img/blackbepoz_bg.png'),
  marker: require('../assets/img/marker.png'),
  barcode: require('../assets/img/barcode-pl.png'),
}
