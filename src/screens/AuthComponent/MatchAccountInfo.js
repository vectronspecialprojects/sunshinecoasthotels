import React, {useReducer, useMemo, useState, useCallback} from 'react'
import {View, StyleSheet, Text, ScrollView, KeyboardAvoidingView} from 'react-native'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import SubHeaderBar from '../../components/SubHeaderBar'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import ButtonView from '../../components/ButtonView'
import TermAndCondition from '../../components/TermAndCondition'
import {isIOS, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {isMultiVenue} from '../../constants/env'
import Dropdown from '../../components/Dropdown'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'
import Input from '../../components/Input'
import {signupMatch} from '../../store/actions/authServices'
import {getStatusBarHeight} from '../../utilities/utils';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

function MatchAccountInfo(props) {
  const venues = useSelector((state) => state.infoServices.venues)
  const dispatch = useDispatch()
  const {data} = props.route?.params || {}
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      venue: '',
      first_name: data?.FirstName || '',
      last_name: data?.LastName || '',
      email: data?.Email1st || '',
      password: '',
      password_confirmation: '',
    },
    inputValidities: {
      venue: !isMultiVenue,
      first_name: !!data?.FirstName,
      last_name: !!data?.LastName,
      email: !!data?.Email1st,
      password: false,
      password_confirmation: false,
    },
    formIsValid: false,
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        input: inputIdentifier,
        value: inputValue,
        isValid: inputValidity,
      })
    },
    [dispatchFormState],
  )

  const dropDownItems = useMemo(() => {
    const items = []
    for (const key in venues) {
      items.push({label: venues[key].name, value: venues[key].id})
    }
    return items
  }, [venues])

  async function handleSubmitData() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      await dispatch(
        signupMatch({
          AccountID: data?.AccountID,
          AccNumber: data?.AccNumber,
          CardNumber: data?.CardNumber,
          share_info: true,
          venue_id: formState.inputValues.venue,
          first_name: formState.inputValues.first_name,
          last_name: formState.inputValues.last_name,
          email: formState.inputValues.email,
          password: formState.inputValues.password,
          password_confirmation: formState.inputValues.password_confirmation,
        }),
      )
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title="Match My Account" />
      <KeyboardAvoidingView
        style={{paddingHorizontal: responsiveWidth(20)}}
        behavior={isIOS() ? 'padding' : 'height'}
        keyboardVerticalOffset={getStatusBarHeight()}>
        <ScrollView keyboardShouldPersistTaps={'handled'} showsVerticalScrollIndicator={false}>
          {isMultiVenue && (
            <Dropdown
              id="venue"
              required
              onInputChange={inputChangeHandler}
              placeholder="Select your preferred venue"
              items={dropDownItems}
            />
          )}
          <Input
            id="first_name"
            placeholder="First Name"
            alphabet
            keyboardType="default"
            required
            minLength={2}
            autoCapitalize="words"
            errorText="Please enter a valid first name."
            onInputChange={inputChangeHandler}
            initialValue={data?.FirstName || ''}
          />
          <Input
            id="last_name"
            //label="Last Name"
            placeholder="Last Name"
            alphabet
            keyboardType="default"
            required
            minLength={2}
            autoCapitalize="words"
            errorText="Please enter a valid last name."
            onInputChange={inputChangeHandler}
            initialValue={data?.LastName || ''}
          />
          <Input
            id="email"
            //label="E-Mail"
            placeholder="E-Mail"
            keyboardType="email-address"
            required
            email
            emailAvailability
            autoCapitalize="none"
            errorText="Please enter a valid email address."
            onInputChange={inputChangeHandler}
            initialValue={data?.Email1st || ''}
          />
          <Input
            id="password"
            //label="Password"
            placeholder="Password"
            keyboardType="default"
            secureTextEntry
            required
            password
            autoCapitalize="none"
            errorText="Password must be not less than 6 characters with at least 1 letter & 1 number."
            onInputChange={inputChangeHandler}
            initialValue=""
          />
          <Input
            id="password_confirmation"
            //label="Confirm Password"
            placeholder="Confirm Password"
            keyboardType="default"
            secureTextEntry
            required
            resetEvent={formState.inputValues.password.length <= 1}
            confirmPassword
            crossCheckPassword={formState.inputValues.password}
            autoCapitalize="none"
            errorText="Please enter the same password"
            onInputChange={inputChangeHandler}
            initialValue=""
          />
          <View style={styles.buttonContainer}>
            <ButtonView
              disabled={!formState.formIsValid}
              title={'Continue'}
              onPress={() => handleSubmitData()}
            />
            <ButtonView
              title={'Cancel'}
              style={styles.buttonBorder}
              titleStyle={{color: Colors.second}}
              onPress={() => {
                props.navigation.pop()
              }}
            />
          </View>
          <TermAndCondition />
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  buttonBorder: {
    borderWidth: 2,
    borderColor: Colors.second,
    backgroundColor: Colors.defaultBackground,
    marginVertical: responsiveHeight(10),
  },
  buttonContainer: {
    marginTop: responsiveHeight(15),
    marginBottom: responsiveHeight(10),
  },
})

export default MatchAccountInfo
