import React from 'react'
import {View, StyleSheet, ImageBackground, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import Colors from '../../../Themes/Colors'
import {TouchableCmp} from '../../../components/UtilityFunctions'

function OurTeamItem({data, onPress}) {

  return (
    <TouchableCmp onPress={onPress}>
      <View style={styles.container}>
        <ImageBackground source={{uri: data.image_square}} style={styles.image}>
          <View style={styles.contentWrapper}>
            <Text style={styles.title}>{data.name}</Text>
            <Text style={styles.content}>{data.venue_name || data.venue?.name}</Text>
          </View>
        </ImageBackground>
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    borderRadius: responsiveWidth(5),
    backgroundColor: '#fff',
    overflow: 'hidden',
    marginTop: responsiveHeight(20),
    marginRight: responsiveWidth(20),
  },
  image: {
    width: responsiveWidth(150),
    height: responsiveWidth(150),
  },
  contentWrapper: {
    padding: responsiveWidth(8),
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: Colors.opacity,
  },
  title: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.openSansBold,
    textAlign: 'center',
    color: Colors.ourTeam.title,
  },
  content: {
    fontSize: responsiveFont(18),
    color: Colors.ourTeam.title,
    textAlign: 'center',
    fontFamily: Fonts.openSans,
  },
})

export default OurTeamItem
