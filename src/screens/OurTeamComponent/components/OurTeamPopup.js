import React, {useEffect, useState} from 'react'
import {View, StyleSheet, Image, Text} from 'react-native'
import Modal from 'react-native-modal'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import fonts from '../../../Themes/Fonts'
import {onLinkPress} from '../../../components/UtilityFunctions'
import Html from '../../../components/Html'
import ButtonView from '../../../components/ButtonView'

function OurTeamPopup({isVisible, item, onPress}) {
  const [data, setData] = useState(item)

  useEffect(() => {
    if (item) {
      setData(item)
    }
  }, [item])

  return (
    <Modal isVisible={isVisible} animationIn="swing" animationOut="zoomOut">
      <View style={styles.popupContainer}>
        <View style={{width: '100%', aspectRatio: 1}}>
          <Image style={styles.image} source={{uri: data?.image_square}} />
        </View>
        <View style={{padding: responsiveWidth(20)}}>
          <Text style={styles.staffName}>{data?.name}</Text>
          <Text style={styles.venueName}>{data?.venue_name}</Text>
          <Html html={`<div>${data?.desc_long}</div>`} textAlign={'center'} color={Colors.ourTeam.description} onLinkPress={onLinkPress}/>
        </View>
        <ButtonView
          onPress={onPress}
          title={'Close'}
          titleStyle={{color: Colors.ourTeam.buttonTitle}}
          style={{backgroundColor: Colors.ourTeam.buttonBackground, borderRadius: 0}}
        />
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  popupContainer: {
    alignSelf: 'center',
    width: deviceWidth() * 0.7,
    backgroundColor: Colors.ourTeam.background,
    borderRadius: responsiveWidth(15),
    overflow: 'hidden',
  },
  image: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover',
  },
  staffName: {
    fontFamily: fonts.openSansBold,
    fontSize: responsiveFont(16),
    color: Colors.ourTeam.title,
    textAlign: 'center',
  },
  venueName: {
    fontFamily: fonts.openSansBold,
    fontSize: responsiveFont(14),
    color: Colors.ourTeam.name,
    textAlign: 'center',
    marginTop: responsiveHeight(10),
  },
})

export default OurTeamPopup
