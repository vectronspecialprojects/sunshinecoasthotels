import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import ScratchImageView from 'react-native-scratch-view'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {responsiveFont} from '../../Themes/Metrics'
import Images from '../../Themes/Images';

function ScratchGameScreen() {
  return (
    <View style={styles.container}>
      <ScratchImageView
        style={{height: 200, width: 350}}
        onRevealPercentChanged={this.onRevealPercentChanged}
        autoRevealPercentage={1}
        strokeWidth={10}
        spotRadius={5}
        scratchImage={Images.grayImage}>
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <Text style={{fontSize: responsiveFont(30)}}>Good Luck!</Text>
        </View>
      </ScratchImageView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default ScratchGameScreen
