import React, {useState, useCallback, useMemo} from 'react'
import {View, FlatList, StyleSheet, Text, RefreshControl} from 'react-native'
import {useSelector, useDispatch} from 'react-redux'
import * as infoServicesActions from '../../store/actions/infoServices'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Fonts from '../../Themes/Fonts'
import ItemList from './ItemList'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import PreferredVenueButton from '../../components/PreferredVenueButton'
import ButtonView from '../../components/ButtonView'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'

const PreferredVenueScreen = (props) => {
  const venues = useSelector((state) => state.infoServices.venues)
  const venueTags = useSelector((state) => state.infoServices.venueTags)
  const venueTagsSetting = useSelector((state) => state.infoServices.venueTagsSetting)
  const profile = useSelector((state) => state.infoServices.profile)
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [selectedTag, setSelectedTag] = useState(null)
  const [selectedVenue, setSelectedVenue] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    setIsRefreshing(true)
    try {
      await dispatch(infoServicesActions.fetchVenues())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
    }
  }, [dispatch])

  const tagHandler = (tag) => {
    if (selectedTag === tag.id) {
      setIsOpen(false)
      setSelectedTag(null)
    } else {
      setIsOpen(true)
      setSelectedTag(tag.id)
    }
  }

  const showData = useMemo(() => {
    let venuesList = []
    if (selectedTag === 1 || venueTagsSetting?.isTagEnabled === 'false') return venues
    venues.forEach((venue) => {
      if (venue?.venue_pivot_tags.filter((tag) => tag?.venue_tag_id === selectedTag).length > 0)
        venuesList = venuesList.concat(venue)
    })
    return venuesList
  }, [venues, venueTags, selectedTag])

  const savePreferredVenue = async () => {
    setGlobalIndicatorVisibility(true)
    if (selectedVenue) {
      try {
        const rest = await dispatch(infoServicesActions.changePreferredVenue(selectedVenue))
        Toast.success(rest)
        props.navigation.pop()
      } catch (err) {
        Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
      }
    } else Alert.alert(localize('sorry'), localize('plsSelectPreferred'), [{text: localize('okay')}])
    setGlobalIndicatorVisibility(false)
  }

  const renderVenuesItem = (itemData) => {
    const venue = itemData.item
    return (
      <ItemList
        style={[styles.venue, selectedVenue === venue.id && {...styles.venueActive}]}
        textStyle={selectedVenue === venue.id && styles.venueActiveText}
        data={venue}
        selectedItem={selectedTag}
        isArrowShowed={false}
        onPress={() => setSelectedVenue(venue.id)}
      />
    )
  }

  const renderVenueList = () => {
    return (
      <View style={{marginTop: responsiveHeight(21)}}>
        <FlatList
          data={showData}
          renderItem={renderVenuesItem}
          keyExtractor={(item) => item.id.toString()}
          ListEmptyComponent={
            <Text style={Styles.flatlistNoItems}>No venues found, please check again later.</Text>
          }
        />
      </View>
    )
  }

  const renderItem = (itemData) => {
    const tag = itemData.item
    return (
      <View>
        <ItemList
          data={tag}
          selectedItem={selectedTag}
          isOpen={isOpen}
          onPress={tagHandler.bind(this, tag)}
          isArrowShowed={true}
          textStyle={{color: Colors.preferredVenue.title}}
        />
        {selectedTag === tag.id && isOpen && renderVenueList()}
      </View>
    )
  }

  return (
    <View style={Styles.screen}>
      <PreferredVenueButton
        icon="down"
        venueName={profile?.member?.current_preferred_venue_name}
        ifOnlineOrdering={profile?.member?.current_preferred_venue_full?.your_order_integration}
        onPress={() => {
          props.navigation.pop()
        }}
      />
      <View style={{flex: 1}}>
        {venueTagsSetting?.isTagEnabled !== 'false' ? (
          <FlatList
            data={venueTags}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
            ListEmptyComponent={
              <Text style={Styles.flatlistNoItems}>No venues found, please check again later.</Text>
            }
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={loadContent}
                tintColor={Colors.defaultRefreshSpinner}
                titleColor={Colors.defaultRefreshSpinner}
                title={localize('pullToRefresh')}
              />
            }
          />
        ) : (
          renderVenueList()
        )}
      </View>
      <View style={{marginHorizontal: responsiveWidth(24), marginVertical: responsiveHeight(10)}}>
        <ButtonView title={localize('saveChanges')} onPress={savePreferredVenue} />
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  venue: {
    paddingLeft: responsiveWidth(50),
    marginTop: responsiveHeight(0),
    height: responsiveHeight(45),
    backgroundColor: Colors.preferredVenue.background,
  },
  venueActive: {
    backgroundColor: Colors.preferredVenue.venueActive,
  },
  venueActiveText: {
    color: Colors.venueActiveText,
    fontFamily: Fonts.openSansBold,
  },
})

export default PreferredVenueScreen
