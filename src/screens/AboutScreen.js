import React, {useEffect, useCallback, useState} from 'react'
import {View, StyleSheet, ScrollView, RefreshControl} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {onLinkPress} from './../components/UtilityFunctions'
import Html from '../components/Html'
import * as infoServicesActions from '../store/actions/infoServices'
import SubHeaderBar from './../components/SubHeaderBar'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import analytics from '@react-native-firebase/analytics'
import {responsiveHeight} from '../Themes/Metrics'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig';

const AboutScreen = (props) => {
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const about = useSelector((state) => state.infoServices.about)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchAbout())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    setIsRefreshing(false)
    dispatch(setGlobalIndicatorVisibility(false))
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  useEffect(() => {
    async function firebase() {
      await analytics().logScreenView({screen_name: 'About'})
    }
    firebase()
  }, [])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors.defaultRefreshSpinner}
            titleColor={Colors.defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }>
        <View style={{flex: 1, paddingHorizontal: 20, marginTop: responsiveHeight(20)}}>
          <Html html={about} textAlign={'left'} color={Colors.about.description} onLinkPress={onLinkPress} />
        </View>
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  loader: {
    backgroundColor: Colors.defaultBackground,
    position: 'absolute',
    top: '50%',
    left: '50%',
    right: '50%',
  },
})

export default AboutScreen
