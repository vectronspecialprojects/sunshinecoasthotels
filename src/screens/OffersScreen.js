import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {View, FlatList, Text, RefreshControl} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {wait} from './../components/UtilityFunctions'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import OffersScreenPopup from './../components/OffersScreenPopup'
import VoucherTile from './../components/VoucherTile'
import Filter from './../components/Filter'
import analytics from '@react-native-firebase/analytics'
import Toast from '../components/Toast'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig';

const OffersScreen = (props) => {
  const title = props.route.params.params.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isFilter, setIsFilter] = useState(false)
  const [selectedId, setSelectedId] = useState(useSelector((state) => state.infoServices.preferredVenueId))
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [item, setItem] = useState(null)
  const offers = useSelector((state) => state.infoServices.offers)


  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchOffers())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  useEffect(() => {
    async function firebase() {
      await analytics().logScreenView({screen_name: 'Promotions'})
    }
    firebase()
  }, [])

  const claimOffer = useCallback(async () => {
    setIsPopupVisible(false)
    dispatch(setGlobalIndicatorVisibility(true))
    try {
      const rest = await dispatch(infoServicesActions.claimOffer(item))
      Toast.success(rest)
      loadContent()
    } catch (err) {
      wait(400).then(() => {
        Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
      })
    }
    dispatch(setGlobalIndicatorVisibility(false))
    setItem(null)
  }, [dispatch, item])

  const filterHandler = (venue) => {
    setSelectedId(venue?.id)
    setIsFilter(false)
  }

  const showData = useMemo(() => {
    if (offers === undefined) return []
    if (offers !== undefined) {
      if (selectedId === 0)
        return offers.sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
      return offers
        .filter((data) => data.listing.venue.id === 0 || data.listing.venue.id === selectedId)
        .sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
    }
  }, [offers, selectedId])

  const popupShow = (item) => {
    if (item === 'close') {
      setIsPopupVisible(false)
    } else {
      setIsPopupVisible(true)
      setItem(item)
    }
  }

  const renderGridItem = (itemData) => {
    return (
      <VoucherTile
        imgSource={itemData.item.listing.image_square}
        onPress={() => popupShow(itemData.item.listing)}
        title={itemData.item.listing.heading}
      />
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar
        title={title}
        filterBtn={true}
        onFilterPress={() => {
          setIsFilter(true)
        }}
      />
      <Filter
        isVisible={isFilter}
        backScreenOnPress={() => {
          setIsFilter(false)
        }}
        selectedId={selectedId}
        onFilterPress={filterHandler}
      />
      <FlatList
        numColumns={2}
        columnWrapperStyle={{
          flex: 1,
          justifyContent: 'space-evenly',
        }}
        keyExtractor={(item) => item.id}
        data={showData}
        renderItem={renderGridItem}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>No offers found, please check again later</Text>
        }
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors.defaultRefreshSpinner}
            titleColor={Colors.defaultRefreshSpinner}
            title="pull to refresh"
          />
        }
      />

      <OffersScreenPopup
        isVisible={isPopupVisible}
        renderItem={item}
        onCancelPress={() => popupShow('close')}
        onOkPress={claimOffer}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default OffersScreen
